# SatNOGS kit #

Documentation for SatNOGS reference ground station.

## License

&copy; 2019 [Libre Space Foundation](https://libre.space).

Licensed under the [Creative Commons Attribution-ShareAlike 4.0 License](LICENSE).
